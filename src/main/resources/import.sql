

INSERT INTO user_groups (id, group_name, group_level, group_status) VALUES (1, 'Admin', 1, 1);
INSERT INTO user_groups (id, group_name, group_level, group_status) VALUES (2, 'Special', 2, 1);
INSERT INTO user_groups (id, group_name, group_level, group_status) VALUES (3, 'User', 3, 1);
INSERT INTO user_groups (id, group_name, group_level, group_status) VALUES (4, 'Monitoreo', 4, 1);
INSERT INTO users (id, name, username, password, user_level, image, status, last_login) VALUES (1, "Admin Users", "admin", "d033e22ae348aeb5660fc2140aec35850c4da997", 1, "pzg9wa7o1.jpg", 1, '2021-02-16 16:52:43');
INSERT INTO users (id, name, username, password, user_level, image, status, last_login) VALUES (2, "Javi User", "user", "6f9b9af3cd6e8b8a73c2cdced37fe9f59226e27d", 3, "pzg9wa7o1.jpg", 1, '2021-02-16 16:52:43');
INSERT INTO users (id, name, username, password, user_level, image, status, last_login) VALUES (3, "Marco Monitoreo", "monitoreo", "d89b3918c8fc10bde42688d399b1b405fdd243f8", 4, "pzg9wa7o1.jpg", 1, '2021-02-16 16:52:43');
INSERT INTO users (id, name, username, password, user_level, image, status, last_login) VALUES (4, "Carlos Special", "special", "3f7653fd1dad7989358e00e297563d8252e36674", 2, "pzg9wa7o1.jpg", 1, '2021-02-16 16:52:43');
INSERT INTO users (id, name, username, password, user_level, image, status, last_login) VALUES (5, "Javier gutierrez", "javiko", "604d2498a303e2d36eb10bf23f8d41aa18de95c9", 1, "pzg9wa7o1.jpg", 0, '2021-02-16 16:52:43');
INSERT INTO users (id, name, username, password, user_level, image, status, last_login) VALUES (6, "Marco peralta", "marquiño", "cbc7b80b58885663f77d6d129ee8b0aff272242c", 3, "pzg9wa7o1.jpg", 1, '2021-02-16 16:52:43');
INSERT INTO users (id, name, username, password, user_level, image, status, last_login) VALUES (7, "Marco peralta", "marquí", "cbc7b80b58885663f77d6d129ee8b0aff272242c", 3, "pzg9wa7o1.jpg", 1, '2021-02-16 16:52:43');
